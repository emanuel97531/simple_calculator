The task is to create simple calculator that can handle the following calculation types: ​ plus,
minus, multiplication, division with the latest Symphony framework (in this case 4.1)

As per the .pdf request, I've setup a simple controller in src/Controller/CalculatorController.php
The controller render the index.html.twig from templates/calculator/index.html.twig
After the submit of form, another simple controller from src/Controller/MessageController.php make the calculations and display them. It renders the templates/message/index.html.twig

