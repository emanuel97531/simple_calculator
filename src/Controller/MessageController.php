<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MessageController extends Controller
{
    /**
     * @Route("/message", name="message", methods="POST")
     */
    public function index(Request $request)
    {
        $rez="";
        $number1="";
        $number2="";
        $number1 = $request->get("number1");
        $number2 = $request->get("number2");
        if (is_numeric($number1) && is_numeric($number2)) {
            $operator = $request->get("operation");
            if ($operator=='plus') {
                $rez=$number1 + $number2;
            }
            if ($operator=='minus') {
                $rez=$number1 - $number2;
            }
            if ($operator=='times') {
                $rez=$number1 * $number2;
            }
            if ($operator=='divided by') {
                $rez=$number1 / $number2;
            }
            $answer="{$number1} {$operator} {$number2} equals {$rez}";
        } else {
            $answer='Numeric values are required';
        }


        return $this->render('message/index.html.twig', ["answer"=>$answer]);
    }
}
