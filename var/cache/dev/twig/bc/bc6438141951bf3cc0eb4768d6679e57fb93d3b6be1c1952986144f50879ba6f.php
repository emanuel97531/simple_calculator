<?php

/* calculator/index.html.twig */
class __TwigTemplate_ce2a0e562021ba11f0384192c69c70edf4dcf2e49f58b1a7d556015ec841207b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "calculator/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "calculator/index.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
\t<head>
\t\t<title>Very Simple Calculator - made by Bumbaru Emanuel, for a test for developers</title>
\t\t<meta charset=\"utf-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t\t<link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\" rel=\"stylesheet\">
\t</head>
\t<body>
\t\t<div class=\"container\" style=\"margin-top: 50px\">
\t\t  <div class=\"row\"><h1>Very Simple Calculator (only '+', '-', '*', '/' operators)</h1></div>
\t\t  <div class=\"row\">&nbsp;</div>
\t\t  
\t\t
\t\t  
\t\t    <!-- Calculator form -->
\t\t    <form method=\"post\" action=\"/message\">
\t\t        <input name=\"number1\" type=\"text\" class=\"form-control\" style=\"width: 150px; display: inline\" />
\t\t        <select name=\"operation\">
\t\t        \t<option value=\"plus\">(+) Plus</option>
\t\t            <option value=\"minus\">(-) Minus</option>
\t\t            <option value=\"times\">(*) Times</option>
\t\t            <option value=\"divided by\">(/) Divided By</option>
\t\t        </select>
\t\t        <input name=\"number2\" type=\"text\" class=\"form-control\" style=\"width: 150px; display: inline\" />
\t\t        <input name=\"submit\" type=\"submit\" value=\"Calculate\" class=\"btn btn-primary\" />
\t\t    </form>
\t\t</div>
\t</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "calculator/index.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
\t<head>
\t\t<title>Very Simple Calculator - made by Bumbaru Emanuel, for a test for developers</title>
\t\t<meta charset=\"utf-8\">
\t\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t\t<link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css\" rel=\"stylesheet\">
\t</head>
\t<body>
\t\t<div class=\"container\" style=\"margin-top: 50px\">
\t\t  <div class=\"row\"><h1>Very Simple Calculator (only '+', '-', '*', '/' operators)</h1></div>
\t\t  <div class=\"row\">&nbsp;</div>
\t\t  
\t\t
\t\t  
\t\t    <!-- Calculator form -->
\t\t    <form method=\"post\" action=\"/message\">
\t\t        <input name=\"number1\" type=\"text\" class=\"form-control\" style=\"width: 150px; display: inline\" />
\t\t        <select name=\"operation\">
\t\t        \t<option value=\"plus\">(+) Plus</option>
\t\t            <option value=\"minus\">(-) Minus</option>
\t\t            <option value=\"times\">(*) Times</option>
\t\t            <option value=\"divided by\">(/) Divided By</option>
\t\t        </select>
\t\t        <input name=\"number2\" type=\"text\" class=\"form-control\" style=\"width: 150px; display: inline\" />
\t\t        <input name=\"submit\" type=\"submit\" value=\"Calculate\" class=\"btn btn-primary\" />
\t\t    </form>
\t\t</div>
\t</body>
</html>
", "calculator/index.html.twig", "/var/www/html/simple_calculator/templates/calculator/index.html.twig");
    }
}
