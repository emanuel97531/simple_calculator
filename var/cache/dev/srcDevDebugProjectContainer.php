<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerZHoGfGj\srcDevDebugProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerZHoGfGj/srcDevDebugProjectContainer.php') {
    touch(__DIR__.'/ContainerZHoGfGj.legacy');

    return;
}

if (!\class_exists(srcDevDebugProjectContainer::class, false)) {
    \class_alias(\ContainerZHoGfGj\srcDevDebugProjectContainer::class, srcDevDebugProjectContainer::class, false);
}

return new \ContainerZHoGfGj\srcDevDebugProjectContainer(array(
    'container.build_hash' => 'ZHoGfGj',
    'container.build_id' => '41e25f3c',
    'container.build_time' => 1537988365,
), __DIR__.\DIRECTORY_SEPARATOR.'ContainerZHoGfGj');
